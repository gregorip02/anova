# Análisis de Varianza

Anova [<b>AN</b>alisis <b>O</b>f <b>VA</b>ariance]
es una pequeña aplicación escrita en javascript 
con el fin de determinar si diferentes tratamientos
muestran diferencias significativas o por el contrario
puede suponerse que sus medias poblacionales no difieren.

## Objetivo

El objetivo de esta aplicación es facilitar el
tratamiento de las medias poblacionales. Usamos
ANOVA de un factor cuando queremos saber si 
las medias de una variable son diferentes entre 
los niveles o grupos de otra variable.