/*
|
|---------------------------------------------------------------------------------------
| 																~ A N O V A ~
|---------------------------------------------------------------------------------------
|
| El objetivo de esta aplicación es facilitar el tratamiento de las medias poblacionales. 
| Usamos ANOVA de un factor cuando queremos saber si las medias de una variable son dife-
| rentes entre los niveles o grupos de otra variable.
|
| @author Gregori Piñeres <gregori.pineres02@gmail.com>
|
| @link https://github.com/gregorip02/anova
|
*/

// Variables globales
var num_tratamientos_global = 1;
var num_elementos_global = $('section#content_wrapper_tratamientos').find('input').length;
var anova_version = '0.3';
var anova_dist = '.relase';

// Html
var wrapper_tratamientos = $('section#content_wrapper_tratamientos');
var html_resultados_anova = $('div#resultados_anova');

// Mostrar version al usuario
anova_dist == '.beta' ? $('strong#anova_version').html(anova_version + anova_dist) : $('strong#anova_version').html(anova_version);

/**
 * Redondea un numero decinal.
 * 
 * @param  integer number
 * 
 * @return string
 */
function redoum(number)
{
	var redoum = number.split('.');

	if (redoum[1] == '00')
		return redoum[0];

	else
		return number;
}

/*
* Agrega observaciones a un tratamiento.
*
* @param event
*
* @param response_html
*/
function add_obs(event, response_html)
{
	event.preventDefault();

	var wrapper = $('.' + response_html);

	var num_elementos_local = wrapper.find('input').length;

	var row = wrapper.attr('row');

	var col = num_elementos_local;

	if (num_elementos_local <= 23)
	{
		// Crear e incrementar la observación.
		var input = $('<input>', {'type': 'text', 'class': row + col, 'row': row, 'col': col, 'size': 2, 'value': 0, 'style': 'display:none'});
		num_elementos_global++;
		
		// Agregar la observación.
		wrapper.append(input);
		wrapper.append(' ');

		// Mostrar la observacion
		input.show('blind');
	}
};

/*
* Elimina observaciones de un tratamiento.
*
* @param event
*
* @param response_html
*/
function del_obs(event, response_html)
{
	event.preventDefault();

	var wrapper = $('.' + response_html);
	var num_elementos_local = wrapper.find('input').length;

	if (num_elementos_local > 2)
	{
		var obs = wrapper.find('input')[num_elementos_local - 1];
		$(obs).hide('blind').remove();
		num_elementos_global--;
	}
};

/*
* Crea y agrega un nuevo tratamiento.
*
* @param event
*/
function add_tratamient(event)
{
	event.preventDefault();

	num_tratamientos_global++;

	num_elementos_global = num_elementos_global + 2;

	var front_tratamientos = num_tratamientos_global + 1;

	wrapper_tratamientos
		.append(
			$('<div>', { 'class': 'form-group content_tratamiento'})
		.append(
			$('<div>', { 'class': 'row'})
		.append(
			$('<div>', { 'class': 'col-xs-8 tratamiento_'+num_tratamientos_global, 'row': num_tratamientos_global})
		.append(
			$('<div>', { 'text': 'Tratamiento '+ front_tratamientos,}))
		.append(
			$('<input />', { 'type': 'text', 'size': 2, 'row': num_tratamientos_global, 'col': 0, 'value': 0})).append(' ')
		.append(
			$('<input />', { 'type': 'text', 'size': 2, 'row': num_tratamientos_global, 'col': 1, 'value': 0})).append(' ')
		)
		.append(
			$('<div>', { 'class': 'col-xs-4 text-right'})
		.append(
			$('<button>', { 'class': 'btn btn-default', 'onclick': 'add_obs(event, "tratamiento_'+num_tratamientos_global+'")'})
		.append(
			$('<span>', { 'class': 'glyphicon glyphicon-plus'})
		)).append(' ')
		.append(
			$('<button>', { 'class': 'btn btn-default del', 'onclick': 'del_obs(event, "tratamiento_'+num_tratamientos_global+'")'})
		.append(
			$('<span>', { 'class': 'glyphicon glyphicon-minus'})
		))))
	);
};

/*
* Elimina el ultimo tratamiento creado.
*
* @param event
*/
function del_tratamient(event)
{
	event.preventDefault();

	if(num_tratamientos_global > 1)
	{
		var remove = wrapper_tratamientos.find('div.content_tratamiento')[num_tratamientos_global];
		var inputs = $(remove).find('input').length;

		remove.remove();

	  num_tratamientos_global--;

		num_elementos_global = num_elementos_global - inputs;
	}
};

/*
* Realiza el análisis de varianza de las medias poblacionales.
*
* @param event
*/
function calculate(event)
{
	event.preventDefault();

	var anova_object = new Object({
		'suma'     : 0, 
		'suma2n'   : 0, 
		'tsuma2n'  : 0, 
		'tsuco'		 : 0,
		'sumctotal': 0,
		'sumcintra': 0,
		'sumcentre': 0,
		'gltotal'  : 0,
		'glintra'	 : 0,
		'glentre'  : 0, 
		'cmintra'  : 0,
		'cmentre'  : 0,
		'vrentre'  : 0,
		'vrintra'  : 0,
		'vrtotal'  : 0,
		'fcal'     : 0,
		'fisher'   : 0,
		'observaciones' : 0,
	});
	anova_object.observaciones = num_elementos_global;

	var valores = $('form#formulario_anova').find('input');

	//Creamos la estructura dependiendo del numero de observaciones proporcionadas.
	//
	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		anova_object[i] = [];
	}
	
	//Ahora obtenemos el valor de cada observacion dependiendo de la estructura creada.
	//
	$.each(valores, function(index, element)
	{
		anova_object[$(element).attr('row')][$(element).attr('col')] = parseFloat($(element).val());
	});
	
	// Comenzamos calculando los totales y los cuadrados de los totales 
	// divididos por el número de observaciones:
	// 
	var get_sum_elements = set_sum_elements(anova_object);

	// A continuación, calculamos los cuadrados de las observaciones y su total:
	// 
	var get_cuadrade_obs = set_cuadrade_obs(get_sum_elements);
	
	// A partir de estas cantidades básicas calculamos las Sumas de Cuadrados:
	// 
	var get_sum_cuadrade = set_sum_cuadrade(get_cuadrade_obs);
	
	// Los cuadrados medios serán:
	// 
	var get_cuadrade_med = set_cuadrade_med(get_sum_cuadrade);
	
	// Fnalmente imprimimos el resultado.
	// 
	destruc(get_cuadrade_med);
};

/*
* set_sum_elements: Calcula los totales y los cuadrados de los totales 
* divididos por el número de observaciones.
*
* @param event
*
*/
function set_sum_elements(object)
{
	anova_object = object;

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
	  anova_object[i]['suma'] = 0;

		$.each(anova_object[i], function(index, element)
		{
			anova_object[i]['suma'] = parseFloat((anova_object[i]['suma'] + anova_object[i][index]));
		});

		anova_object[i]['suma2n'] = parseFloat((anova_object[i]['suma'] * anova_object[i]['suma']) / (anova_object[i].length));

		anova_object['suma'] = parseFloat(anova_object['suma'] + anova_object[i]['suma']);
	}

	anova_object.suma2n = parseFloat((anova_object['suma'] * anova_object['suma']) / anova_object.observaciones);

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		anova_object.tsuma2n = parseFloat(anova_object.tsuma2n + anova_object[i]['suma2n']);
	}

	return anova_object;
}

/*
* set_cuadrate_obs: Calcula la suma de las observaciones y su cuadrado.
*
* @param object
*
*/
function set_cuadrade_obs(object)
{
	anova_object = object;

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		anova_object[i]['suco'] = 0;

		$.each(anova_object[i], function(index, element)
		{
			anova_object[i]['suco'] = parseFloat(anova_object[i]['suco'] + (element * element));
		});

		anova_object.tsuco = parseFloat(anova_object.tsuco + anova_object[i]['suco']);
	}

	return anova_object;
}

/*
* set_num_cuadrate: Calcula la suma de los cuadrados.
* 
* @param object
*/
function set_sum_cuadrade(object)
{
	anova_object = object;

	anova_object.sumctotal = parseFloat(anova_object.tsuco - anova_object.suma2n);

	anova_object.sumcintra = parseFloat(anova_object.tsuco - anova_object.tsuma2n);

	anova_object.sumcentre = parseFloat(anova_object.tsuma2n - anova_object.suma2n);

	return anova_object;
}

/*
* set_num_cuadrate: Calcula los medios de los cuadrados.
*										Calcula los grados de libertad.
*										Calcula la varianza.
*										Calcula F.
* @param object
*/
function set_cuadrade_med(object)
{
	html_resultados_anova.html(' ');

	$('#get_sum_elements').html(' ');

	$('#get_cuadrade_obs').html(' ');

	$('#get_sum_cuadrads').html(' ');

	$('#get_cuadrade_med').html(' ');

	anova_object = object;
	/*
	* Calcular los grados de libertad.
	*/
	anova_object.glentre = parseFloat((num_tratamientos_global + 1) - 1);

	anova_object.glintra = parseFloat(anova_object.observaciones - (num_tratamientos_global + 1));

	anova_object.gltotal = parseFloat(anova_object.observaciones - 1);
	/*
	* Calcular los cuadrados medios.
	*/
	anova_object.cmentre = parseFloat(anova_object.sumcentre / anova_object.glintra);

	anova_object.cmintra = parseFloat(anova_object.sumcintra / anova_object.glentre);
	/*
	* Calcular las varianzas.
	*/
	anova_object.vrentre = parseFloat(anova_object.sumcentre / anova_object.glentre);

	anova_object.vrintra = parseFloat(anova_object.sumcintra / anova_object.glintra);

	anova_object.vrtotal = parseFloat(anova_object.sumctotal / anova_object.gltotal);
	/*
	* Calcular F
	*/
	anova_object.vrintra == 0 ? anova_object.fcal = 0 
														: anova_object.fcal = parseFloat(anova_object.vrentre / anova_object.vrintra);

	fisher(anova_object);
	
	return anova_object;
}

/*
* destruc: Crea una estructura de tablas y muestra los resultados.
*
* @param: object
*/
function destruc(object)
{
	anova_object = object;

	html_resultados_anova.html(' ');

	html_resultados_anova
	.append($('<div>', { 'class': 'table-responsive'})
		.append($('<table>', { 'class': 'table table-hover table-bordered'})
			.append($('<thead>')
				.append($('<tr/>' , { 'class': 'default' })
					.append($('<th>', { 'class': 'text-center'}))
					.append($('<th>', { 'class': 'text-center', 'text': 'Suma de cuadrados'}))
					.append($('<th>', { 'class': 'text-center', 'text': 'Grados de libertad'}))
					.append($('<th>', { 'class': 'text-center', 'text': 'Medias cuadraticas'}))
					.append($('<th>', { 'class': 'text-center', 'text': 'Cociente F'}))
				)
			)
			.append($('<tbody>')
				.append($('<tr/>' , { 'class': 'text-center' })
					.append($('<td>', { 'class': 'text-right', 'text': 'Entre' }))
					.append($('<td>', { 'text': redoum(anova_object.sumcentre.toFixed(2)) }))
					.append($('<td>', { 'text': anova_object.glentre }))
					.append($('<td>', { 'text': redoum(anova_object.vrentre.toFixed(2)) }))
					.append($('<td>', { 'text': redoum(anova_object.fcal.toFixed(2)), 'rowspan': 2 }))
				)
				.append($('<tr/>' , { 'class': 'text-center' })
					.append($('<td>', { 'class': 'text-right', 'text': 'Intra' }))
					.append($('<td>', { 'text': redoum(anova_object.sumcintra.toFixed(2)) }))
					.append($('<td>', { 'text': anova_object.glintra }))
					.append($('<td>', { 'text': redoum(anova_object.vrintra.toFixed(2)) }))
				)
				.append($('<tr/>' , { 'class': 'text-center' })
					.append($('<td>', { 'class': 'text-right', 'text': 'Total' }))
					.append($('<td>', { 'text': redoum(anova_object.sumctotal.toFixed(2)) }))
					.append($('<td>', { 'text': anova_object.gltotal }))
					.append($('<td>', { 'colspan': 2 })
						.append($('<a>', { 'href': './fisher.pdf', 'target': '_blank' })
							.append($('<button>', { 'class': 'btn btn-default', 'text': 'Tabla de R. Fisher'}))
						)
					)
				)
			)
		)
	)

	print_results(anova_object);

	// Mostrar pasos
	//
	if (!$("#row_anova_results").is(':visible'))
	{
		$("#row_anova_results").show('blind');
	}
}

/*
* Imprime el procedimiento para llegar a la solución.
*
* @param object
*/
function print_results(object)
{
	anova_object = object;

	/*
	* Comenzamos calculando los totales y los cuadrados de los totales divididos por el
	* número de observaciones:
	*/

	$('#get_sum_elements').append($('<h5>', { 'text': 'Comenzamos calculando los totales y los cuadrados de los totales divididos por el número de observaciones:' }));

	$('#get_sum_elements')
		.append($('<table>', { 'class': 'table table-bordered table-hover', 'id': 'table_get_obs_container'})
			.append($('<thead>')
				.append($('<tr/>' , { 'id': 'tr_get_obs_container'})
					.append($('<th>', { 'class': 'text-center' }))
				)
			)
		);

	for(var i = 1; i <= (num_tratamientos_global + 1); i++)
	{
		$('tr#tr_get_obs_container').append($('<th>', { 'class': 'text-center', 'text': 'Tratamiento ' + i}));
	}

	$('tr#tr_get_obs_container')
		.append($('<th>', { 'class': 'text-center', 'text': 'Total' }))
		.append($('<th>', { 'class': 'text-center', 'text': 'Sum2/n' }));

	$('table#table_get_obs_container')
		.append($('<tbody>' , { 'id': 'tbody_get_obs_container_sum'})
			.append($('<tr/>' , { 'id': 'tr_get_obs_container_sum' })
				.append($('<td>', { 'class': 'text-center', 'text': 'Suma' }))
			)
		)

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		$('tr#tr_get_obs_container_sum').append($('<td>', { 'class': 'text-center', 'text': anova_object[i]['suma'] }));
	}

	$('tr#tr_get_obs_container_sum')
		.append($('<td>', { 'class': 'text-center', 'text': anova_object.suma }))
		.append($('<td>', { 'class': 'text-center', 'text': anova_object.suma2n }));

	$('tbody#tbody_get_obs_container_sum')
		.append($('<tr/>' , { 'id': 'tr_get_obs_container_sum2n'})
			.append($('<td>', { 'class': 'text-center', 'text': 'Suma2/n'}))
	);

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		$('tr#tr_get_obs_container_sum2n').append($('<td>', { 'class': 'text-center', 'text': anova_object[i]['suma2n'] }));
	}

	$('tr#tr_get_obs_container_sum2n').append($('<td>', { 'class': 'text-center', 'text': anova_object.tsuma2n }));

	/*
	* A continuación calculamos los cuadrados de las observaciones y su total:
	*/

	$('div#get_cuadrade_obs').append($('<h5>', { 'text': 'A continuación, calculamos los cuadrados de las observaciones y su total: '}));

	$('#get_cuadrade_obs')
		.append($('<table>', { 'class': 'table table-bordered table-hover', 'id': 'table_get_cuadrade_obs'})
			.append($('<thead>')
				.append($('<tr>' , { 'id': 'tr_get_cuadrade_obs'}))
			)
		);

	for(var i = 1; i <= (num_tratamientos_global + 1); i++)
	{
		$('tr#tr_get_cuadrade_obs').append($('<th>', { 'class': 'text-center', 'text': 'Tratamiento ' + i}));
	}

	$('tr#tr_get_cuadrade_obs').append($('<th>', { 'class': 'text-center', 'text': 'Total'}));

	$('table#table_get_cuadrade_obs')
		.append($('<tbody>', { 'id': 'tbody_get_obs_cuadrade' })
			.append($('<tr>', { 'id': 'tr_get_obs_cuadrade' }))
	);

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		$('tr#tr_get_obs_cuadrade').append($('<td>', { 'class': 'text-center', 'id': 'td_get_obs_cuadrade_'+i }));
		
		$.each(anova_object[i], function(index, element)
		{
			$('td#td_get_obs_cuadrade_'+i).append( parseFloat((anova_object[i][index] * anova_object[i][index])) ).append('<br>');
		});
	}

	$('tbody#tbody_get_obs_cuadrade').append($('<tr>', { 'id': 'get_total_cuadrade_obs'}));

	for(var i = 0; i <= num_tratamientos_global; i++)
	{
		$('tr#get_total_cuadrade_obs').append($('<td>', { 'class': 'text-center', 'text': anova_object[i]['suco'] }));
	}

	$('tr#get_total_cuadrade_obs').append($('<td>', { 'class': 'text-center', 'text': anova_object['tsuco'] }));

	/*
	* A partir de estas cantidades básicas calculamos las Sumas de Cuadrados:
	*/

	$('div#get_sum_cuadrads').append($('<h5>', { 'text': 'A partir de estas cantidades básicas calculamos las Sumas de Cuadrados:' }));

	$('div#get_sum_cuadrads').append($('<div>').append($('<pre>', { 'id': 'get_sum_cuadrade_container'})));

	$('pre#get_sum_cuadrade_container').append('Suma de cuadrados toal : ' + redoum(anova_object.tsuco.toFixed(2)) +' - '+ redoum(anova_object.suma2n.toFixed(2)) +' = '+ redoum(anova_object.sumctotal.toFixed(2))).append('<br>');

	$('pre#get_sum_cuadrade_container').append('Suma de cuadrados intra : ' + redoum(anova_object.tsuco.toFixed(2)) +' - '+ redoum(anova_object.tsuma2n.toFixed(2)) +' = '+ redoum(anova_object.sumcintra.toFixed(2))).append('<br>');

	$('pre#get_sum_cuadrade_container').append('Suma de cuadrados entre : ' + redoum(anova_object.tsuma2n.toFixed(2)) + ' - '+ redoum(anova_object.suma2n.toFixed(2)) +' = '+ redoum(anova_object.sumcentre.toFixed(2))).append('<br>');

	/*
	* Los cuadrados medios serán:
	*/

	$('div#get_cuadrade_med').append($('<h5>' , { 'text': 'Los cuadrados medios serán:' }));
	
	$('div#get_cuadrade_med').append($('<pre>', { 'id': 'get_cuadrade_med_pre' }));
	
	$('pre#get_cuadrade_med_pre').append('Cuadrado medio entre : '+ redoum(anova_object.sumcentre.toFixed(2)) +' / '+ anova_object.glintra +' = '+ redoum(anova_object.cmentre.toFixed(2))).append('<br>');
	
	$('pre#get_cuadrade_med_pre').append('Cuadrado medio intra : '+ redoum(anova_object.sumcintra.toFixed(2)) +' / '+ anova_object.glentre +' = '+ redoum(anova_object.cmintra.toFixed(2))).append('<br>');

	$('pre#get_cuadrade_med_pre').append('F : '+ anova_object.vrentre +' / '+ redoum(anova_object.vrintra.toFixed(2)) +' = '+ redoum(anova_object.fcal.toFixed(2)));

	console.log(anova_object);

}

/*
* Mostrar los pasos para resolver anova.
*
* @param event
*/
$('button#button_show_procedimients').click(function(event)
{
	event.preventDefault();

	if (!$('div#show_procedimients').is(':visible'))
	{
		$(this).text('Ocultar pasos');

		$('div#show_procedimients').show('fast');
	}

	else
	{
		$(this).text('Mostrar pasos');

		$('div#show_procedimients').hide('fast');
	}
});

/**
 * Lee el archivo fisher.json y calcula la hipotesis nula.
 * 
 * @return integer
 */
function fisher(anova_object)
{
	var anova_object = anova_object;

	var confianza = $('select#anova_confianza').val();
	var numerador = anova_object.glintra;
	var denominador = anova_object.glentre;

	$.getJSON('./fisher.json', function(fisher, response)
	{
		var fisher = fisher[confianza][numerador][denominador-1];

		if (fisher > anova_object.fcal)
		{
			$('div.well').html('El valor de la <b>F: '+ anova_object.fcal +'</b> teórica con '+ denominador +' y '+ numerador +' grados de libertad, a un nivel de confianza de '+ confianza +' es '+ fisher +'. Por consiguiente no se rechaza la hipótesis nula. No existiendo diferencias significativas entre las medias poblacionales.');
		}
		else
		{
			$('div.well').html('El valor de la <b>F: '+ anova_object.fcal +'</b> teórica con '+ denominador +' y '+ numerador +' grados de libertad, a un nivel de confianza de '+ confianza +' es '+ fisher +'. Por consiguiente se rechaza la hipótesis nula. Existen por tanto diferencias significativas entre las medias poblacionales.');
		}
	});
}